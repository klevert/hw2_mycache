import pytest

from main import mycache


def test_use_cache_true():
    '''Полооожительный сценарий'''

    @mycache(use_cache=True)
    def fibonacci(n):
        if n < 2: return n
        return fibonacci(n - 1) + fibonacci(n - 2)

    res = fibonacci(20)
    cache = fibonacci.cache
    count = fibonacci.count

    assert res == 6765
    assert len(cache) == 21
    assert count == 21


def test_without_use_cache():
    '''Полооожительный сценарий'''

    @mycache()
    def fibonacci(n):
        if n < 2: return n
        return fibonacci(n - 1) + fibonacci(n - 2)

    res = fibonacci(20)
    cache = fibonacci.cache
    count = fibonacci.count

    assert res == 6765
    assert len(cache) == 21
    assert count == 21


def test_use_cache_false():
    '''Полооожительный сценарий'''

    @mycache(use_cache=False)
    def fibonacci(n):
        if n < 2: return n
        return fibonacci(n - 1) + fibonacci(n - 2)

    res = fibonacci(20)
    cache = fibonacci.cache
    count = fibonacci.count

    assert res == 6765
    assert len(cache) == 21
    assert count == 21891


def test_step_not_integer():
    '''Негативный сценарий'''

    with pytest.raises(TypeError) as e:
        @mycache(use_cache=123)
        def fibonacci(n):
            if n < 2: return n
            return fibonacci(n - 1) + fibonacci(n - 2)

        fibonacci(20)

    assert str(e.value) == "Expected use_cache to be a boolean"
