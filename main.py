import time


def mycache(use_cache: bool = True):
    '''Cache decorator'''
    if not isinstance(use_cache, bool):
        raise TypeError(
            'Expected use_cache to be a boolean')

    def decorator(func):
        cache = {}

        def wrapper(*args: tuple, **kwargs: dict):

            key = args
            if kwargs:
                for item in kwargs.items():
                    key += item

            if use_cache:
                if key in cache:
                    res = cache[key]
                else:
                    res = func(*args, **kwargs)
                    cache[key] = res
                    wrapper.count = wrapper.count + 1 if hasattr(wrapper, 'count') else 1  # для тестирования

            else:
                res = func(*args, **kwargs)
                cache[key] = res
                wrapper.count = wrapper.count + 1 if hasattr(wrapper, 'count') else 1  # для тестирования
            wrapper.cache = cache  # для тестирования
            return res

        return wrapper

    return decorator
